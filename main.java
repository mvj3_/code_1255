import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class Main extends Activity {
private final String mapSearchIntent = "com.decarta.mapsearch.intent.action.SEARCH";

/** Called when the activity is first created. */

@Override
public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.main);
Uri mapUri = Uri.parse("geo:39.906033,116.397700");
Intent i = new Intent(mapSearchIntent, mapUri);
i.setData(mapUri);
startActivity(i);
}
}